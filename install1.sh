#!/bin/sh

PROJECTDIR=$(dirname "$(readlink -f "$0")")

#default
ESP=/dev/vda1
ROOT=/dev/vda2
FORMAT_ESP=false
FORMAT_ROOT=false
LOCALTIME=Asia/Kolkata
ROOTPASS=coolvm
HOSTNAME=titan
ON_SINGLE_PARTITION=false

function print_help(){
    echo "check the script you lazy but good person"
    exit -1
}

. $PROJECTDIR/packages.sh

options=$(getopt -o '' --long root-pass:,root:,esp:,format-esp,format-root,on-single-partition,server:,hostname: -- "$@")

error_code=$?
if [ "$error_code" != "0" ]; then
    echo "give valid arguments"
    print_help
    exit -1;
fi

eval set -- "$options"
while true; do
    case "$1" in
        --root)        
            shift
            ROOT=$1
            ;;
        --esp) 
            shift
            ESP=$1
            ;;
        --on-single-partition) 
            ON_SINGLE_PARTITION=true
            ;;
        --server)      
            shift 
            SERVER="$1" 
            ;;
        --format-esp)
            FORMAT_ESP=true
            ;;
        --format-root) 
            FORMAT_ROOT=true
            ;;
        --help)
            print_help
            exit
            ;;
        --root-pass)
            shift
            ROOT_PASS=$1
            ;;
        --hostname)
            shift
            HOSTNAME=$1
            ;;
        --localtime)
            shift
            LOCALIME=$1
            ;;
        --)
            shift;
            break ;;
    esac
    shift;
done


PROJECTDIR=$(dirname "$(readlink -f "$0")")

if [ -n "$SERVER" ]; then
    mv /etc/pacman.conf /etc/pacman.conf.old
    cp $PROJECTDIR/pacman.conf /etc/
    sed -i -e "s~MY_SERVER~Server = $SERVER~g" /etc/pacman.conf
fi

timedatectl set-ntp true

umount $ESP > /dev/null  2>&1
umount $ROOT > /dev/null 2>&1

if [ $FORMAT_ROOT = true ]; then
    echo "Formatting $ROOT"
    echo y | mkfs.ext4 $ROOT 
fi

if [ $FORMAT_ESP = true ]; then
    echo "Formatting $ESP"
    mkfs.fat -F32 $ESP
fi

echo "mounting $ROOT"
mount $ROOT /mnt 

if [ $ON_SINGLE_PARTITION = false ]; then
    mkdir /mnt/esp
    echo "mounting $ESP"
    mount $ESP /mnt/esp
fi

pacstrap /mnt $IMP_PACKAGES $PACKAGES || { echo 'pacstrap failed' ; exit -1; }
genfstab -U /mnt >> /mnt/etc/fstab 

cp $PROJECTDIR/install2.sh /mnt

#copy all dotfiles to /mnt/root
for x in $PROJECTDIR/dotfiles/.[!.]*; do
  if [ -e "$x" ]; then cp "$x" /mnt/root ; fi
done

echo "setting up vim"

curl -fLo /mnt/root/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "setting up zsh"

if [ ! -x "$(command -v $x)" ]; then
    pacman -S git
fi

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git /mnt/opt/powerlevel10k

arch-chroot /mnt /bin/sh -c "/install2.sh \"$ROOTPASS\" \"$HOSTNAME\" \"$LOCALTIME\" \"$ON_SINGLE_PARTITION\" \"$ROOT\" " 

echo "done"
