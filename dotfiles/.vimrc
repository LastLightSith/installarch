set tabstop=4
set shiftwidth=4
set expandtab

set relativenumber
set rnu

"let b:sql_type_override='pgsql' | set ft=sql

call plug#begin('~/.vim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'

Plug 'vim-airline/vim-airline-themes'
Plug 'vim-airline/vim-airline'
Plug 'vim-syntastic/syntastic'

"theme
Plug 'aonemd/kuroi.vim'
Plug 'henrynewcomer/vim-theme-papaya'

"icons
Plug 'ryanoasis/vim-devicons'

Plug 'wesQ3/vim-windowswap'
Plug 'raimondi/delimitmate' ",{'for':['java','groovy']}
Plug 'kana/vim-fakeclip'

"fish
Plug 'dag/vim-fish'

"QML
Plug 'peterhoeg/vim-qml'

"JSON
Plug 'elzr/vim-json'

"Kotlin
Plug 'udalov/kotlin-vim'

"pgsql
Plug 'lifepillar/pgsql.vim'

"Godot
Plug 'calviken/vim-gdscript3'

"Rust
Plug 'rust-lang/rust.vim'
Plug 'ron-rs/ron.vim'

"glsl
Plug 'tikhomirov/vim-glsl'

"haxe
Plug 'jdonaldson/vaxe'

"crystal
Plug 'rhysd/vim-crystal'


call plug#end()

let g:closetag_filenames = '*.html,*.xhtml,*.phtmal,*php,*.xml'

"set background=dark
colorscheme papaya

hi Normal guibg=NONE ctermbg=NONE
highlight NonText ctermbg=none

set completeopt=menuone,noselect,noinsert
"

"syntastic recomended
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_mode_map = { 'passive_filetypes': ['python'] }
let g:syntastic_loc_list_height=3
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:airline_powerline_fonts = 1
