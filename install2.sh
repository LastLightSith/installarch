#!/bin/bash
ROOTPASS=$1
HOSTNAME=$2
LOCALTIME=$3
ON_SINGLE_PARTITION=$4
ROOT=$5

rm /usr/bin/sh
ln -s /usr/bin/dash /usr/bin/sh

echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
locale-gen

echo $HOSTNAME > /etc/hostname
echo "127.0.0.1 localhost\n::1 localhost" > /etc/hosts

ln -sf /usr/share/zoneinfo/$LOCALTIME /etc/localtime 
hwclock --systohc

gem install bundler

#setup postgresql
sudo -u postgres initdb -D /var/lib/postgres/data

mkinitcpio -P

if [ $ON_SINGLE_PARTITION = true ]; then
    grub-install --target=i386-pc $ROOT
else
    grub-install --target=x86_64-efi --efi-directory=esp --bootloader-id=GRUB
fi

grub-mkconfig -o /boot/grub/grub.cfg

echo "set root password"
echo root:$ROOTPASS | chpasswd

#for KVM Access
#systemctl enable serial-getty@ttyS0.service

systemctl enable NetworkManager
systemctl enable firewalld.service

echo "done. you can reboot now"
